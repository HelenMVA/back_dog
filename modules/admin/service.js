import AdminQueries from "./query";
// import mv from "mv";

const AdminServices = {
  getAll: (req, callback) => {
    AdminQueries.getAll(
      req,
      (response) => {
        return callback({ success: true, message: response });
      },
      (error) => {
        return callback({ success: false, message: error });
      }
    );
  },
  acceptDemand: (req, callback) => {
    AdminQueries.acceptDemand(
      req,
      (response) => {
        return callback({ success: true, message: response });
      },
      (error) => {
        return callback({ success: false, message: error });
      }
    );
  },

  availableDog: (req, callback) => {
    AdminQueries.availableDog(
      req,
      (response) => {
        return callback({ success: true, message: response });
      },
      (error) => {
        return callback({ success: false, message: error });
      }
    );
  },

  rejectDemand: (req, callback) => {
    AdminQueries.rejectDemand(
      req,
      (response) => {
        return callback({ success: true, message: response });
      },
      (error) => {
        return callback({ success: false, message: error });
      }
    );
  },
  // UPLOAD IMG

  adoptannonce: (req, callback) => {
    var file = req.files.file;
    var img_name = req.files.file.name;
    file.mv('./public/img/upload_img/' + file.name)
    AdminQueries.adoptannonce(img_name, (response) => {
      return callback({ success: true, message: response });
    },
      (error) => {
        return callback({ success: false, message: error });
      })

  },
};

export default AdminServices;
