import express from "express";
const router = express.Router();

import RateController from "./controller";

router.post("/:id", RateController.rateUser);
router.get("/user/:id", RateController.getAllRateByUser);
router.put("/user/:id", RateController.putUserRate);
router.get("/top3", RateController.getTopUser);



export default router;
