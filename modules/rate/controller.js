import RateServices from "./service";


const RateController = {
    rateUser: (req, res) => {
        RateServices.rateUser(req, (result) => {
            result.success
                ? res.status(201).send(result)
                : res.status(404).send(result);
        });
    },

    getAllRateByUser: (req, res) => {
        RateServices.getAllRateByUser(req, (result) => {
            result.success
                ? res.status(201).send(result)
                : res.status(404).send(result);
        });
    },
    putUserRate: (req, res) => {
        RateServices.putUserRate(req, (result) => {
            result.success
                ? res.status(201).send(result)
                : res.status(404).send(result);
        });
    },
    getTopUser: (req, res) => {
        RateServices.getTopUser(req, (result) => {
            result.success
                ? res.status(201).send(result)
                : res.status(404).send(result);
        });
    },
};

export default RateController;
