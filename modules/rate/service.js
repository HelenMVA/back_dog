import RateQueries from "./query";

const RateServices = {
    rateUser: (req, callback) => {
        RateQueries.rateUser(
            req,
            (response) => {
                return callback({ success: true, message: response });
            },
            (error) => {
                return callback({ success: false, message: error });
            }
        );
    },
    getAllRateByUser: (req, callback) => {
        RateQueries.getAllRateByUser(
            req,
            (response) => {
                return callback({ success: true, message: response });
            },
            (error) => {
                return callback({ success: false, message: error });
            }
        );
    },
    putUserRate: (req, callback) => {
        RateQueries.putUserRate(
            req,
            (response) => {
                return callback({ success: true, message: response });
            },
            (error) => {
                return callback({ success: false, message: error });
            }
        );
    },

    getTopUser: (req, callback) => {
        RateQueries.getTopUser(
            req,
            (response) => {
                return callback({ success: true, message: response });
            },
            (error) => {
                return callback({ success: false, message: error });
            }
        );
    },
};

export default RateServices;
