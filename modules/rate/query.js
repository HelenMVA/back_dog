import db from "../../setup/database";

const Queries = {

    rateUser: (req, successCallback, failureCallback) => {
        let sqlQuery = `INSERT INTO user_rating
        (id ,owner_user_id, serve_user_id, id_demand, value) 
        VALUES (NULL ,"${req.body.owner_user_id}", "${req.body.serve_user_id}","${req.params.id}", "${req.body.value}")`;

        db.query(sqlQuery, (err) => {
            if (err) {
                return failureCallback(err);
            } else {
                return successCallback(req.body);
            }
        });
    },

    getAllRateByUser: (req, successCallback, failureCallback) => {
        let sqlQuery = `SELECT * FROM user_rating WHERE serve_user_id= "${req.params.id}"`;

        db.query(sqlQuery, (err, rows) => {
            if (err) {
                return failureCallback(err);
            }
            if (rows.length > 0) {
                return successCallback(rows);
            } else {
                return successCallback("No rate by this user");
            }
        });
    },
    putUserRate: (req, successCallback, failureCallback) => {
        let id = req.params.id;
        let rating = req.body.rating;
        let sqlQuery = `UPDATE users
                         SET rating=${rating}
                         WHERE id=${id}`;
        db.query(sqlQuery, (err, rows) => {
            if (err) {
                return failureCallback(err);
            } else {
                return successCallback("user rate update");
            }
        });
    },

    getTopUser: (req, successCallback, failureCallback) => {
        let sqlQuery = `SELECT id, firstname, lastname, created_date, avatar_url, rating
        FROM users
        WHERE visibility = 1
        ORDER BY rating DESC LIMIT 3`;

        db.query(sqlQuery, (err, rows) => {
            if (err) {
                return failureCallback(err);
            }
            if (rows.length > 0) {
                return successCallback(rows);
            } else {
                return successCallback("No users");
            }
        });
    },

};

export default Queries;
