import express from "express";
const router = express.Router();

import UserController from "./controller";

//Public routes
router.post("/authenticate", UserController.authenticate);
router.post("/register", UserController.register);

/**
 * user adopt dog demandes
 */
router.get("/:id", UserController.getUserDog);
router.put("/rejectMyAdoptDemand/:id", UserController.rejectMyAdoptDemand);

/**
 * user keep dog demandes
 */
router.get("/keepDog/:id", UserController.getUserKeepDog);
router.get("/keepDog/accept/:id", UserController.getUserAcceptKeepDog);


/**
 * USER data
 */
router.get("/me/:id", UserController.getUserData);
router.put("/deleteAvatar/:id", UserController.deleteAvatar);
router.put("/editAvatar/:id", UserController.editAvatar);

/**
 * User Messages
 */
router.post("/message", UserController.message);
router.get("/messages/:id/:fromid", UserController.getUserMessages);
router.get("/myusers/:id/", UserController.getMyUsers);

/**
 * recuprer current annonces by user 
 * er 
 * current demandes pour ces anonces
 */
router.post("/currentAnnonces/:id", UserController.getMyCurrentAnnonces);
router.get("/currentDemandes/:id", UserController.getCurrentDemandes);


/**
 * recuprer passe annonces by user 
 * 
 */
router.post("/passAnnonces/:id", UserController.getMyPassAnnonces);
router.post("/findRateByUser", UserController.findRateByUser);

/**
 * Reject sois accept demand a garder le chien 
 */
router.put("/keepDemand/editStatus", UserController.editStatusDemand);
router.put("/currentAnnonces/availability", UserController.editDogAvailability);


export default router;
