import UserServices from "./service";

const UserController = {
  getUserWithToken: (req, res) => {
    UserServices.getUserWithToken(req).then((result) =>
      res.status(result.status).send(result.payload)
    );
  },
  authenticate: (req, res) => {
    UserServices.authenticate(req.body).then((result) =>
      res.status(result.status).send(result.payload)
    );
  },
  register: async (req, res) => {
    UserServices.register(req.body).then((result) =>
      res.status(result.status).send(result.payload)
    );
  },
  getUserData: (req, res) => {
    UserServices.getUserData(req, (result) => {
      result.success
        ? res.status(200).send(result)
        : res.status(404).send(result);
    });
  },
  getUserDog: (req, res) => {
    UserServices.getUserDog(req, (result) => {
      result.success
        ? res.status(200).send(result)
        : res.status(404).send(result);
    });
  },
  getUserKeepDog: (req, res) => {
    UserServices.getUserKeepDog(req, (result) => {
      result.success
        ? res.status(200).send(result)
        : res.status(404).send(result);
    });
  },
  getUserAcceptKeepDog: (req, res) => {
    UserServices.getUserAcceptKeepDog(req, (result) => {
      result.success
        ? res.status(200).send(result)
        : res.status(404).send(result);
    });
  },

  /**
   * 
   * USER messages
   */
  message: (req, res) => {
    UserServices.message(req, (result) => {
      result.success
        ? res.status(201).send(result)
        : res.status(404).send(result);
    });
  },
  getUserMessages: (req, res) => {
    UserServices.getUserMessages(req, (result) => {
      result.success
        ? res.status(200).send(result)
        : res.status(404).send(result);
    });
  },
  getMyUsers: (req, res) => {
    UserServices.getMyUsers(req, (result) => {
      result.success
        ? res.status(200).send(result)
        : res.status(404).send(result);
    });
  },

  /**
   * User Adopt demandes
   */
  rejectMyAdoptDemand: (req, res) => {
    UserServices.rejectMyAdoptDemand(req, (result) => {
      //Will be executed once the service is finished
      result.success
        ? res.status(200).send(result)
        : res.status(404).send(result);
    });
  },

  /**
   * My current annonces a garder
   */
  getMyCurrentAnnonces: (req, res) => {
    UserServices.getMyCurrentAnnonces(req, (result) => {
      result.success
        ? res.status(200).send(result)
        : res.status(404).send(result);
    });
  },
  getCurrentDemandes: (req, res) => {
    UserServices.getCurrentDemandes(req, (result) => {
      result.success
        ? res.status(200).send(result)
        : res.status(404).send(result);
    });
  },
  editStatusDemand: (req, res) => {
    UserServices.editStatusDemand(req, (result) => {
      result.success
        ? res.status(200).send(result)
        : res.status(404).send(result);
    });
  },

  editDogAvailability: (req, res) => {
    UserServices.editDogAvailability(req, (result) => {
      result.success
        ? res.status(200).send(result)
        : res.status(404).send(result);
    });
  },


  /**
   * Pass Annonces
   */

  getMyPassAnnonces: (req, res) => {
    UserServices.getMyPassAnnonces(req, (result) => {
      result.success
        ? res.status(200).send(result)
        : res.status(404).send(result);
    });
  },



  /**
   * On check si on rate deja user ou non
   */
  findRateByUser: (req, res) => {
    UserServices.findRateByUser(req, (result) => {
      result.success
        ? res.status(200).send(result)
        : res.status(404).send(result);
    });
  },


  /**
   * USER avatar
   */
  deleteAvatar: (req, res) => {
    UserServices.deleteAvatar(req, (result) => {
      result.success
        ? res.status(200).send(result)
        : res.status(404).send(result);
    });
  },
  editAvatar: (req, res) => {
    UserServices.editAvatar(req, (result) => {
      result.success
        ? res.status(200).send(result)
        : res.status(404).send(result);
    });
  },
};

export default UserController;
